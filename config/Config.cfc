component {
    
    public void function configure( required struct config ) {

        var settings = arguments.config.settings ?: {};

        settings.features.dumpLogs = { enabled=true, siteTemplates=[ "*" ], widgets=[] };

        settings.adminConfigurationMenuItems.append( "dumpLogs" );

        settings.adminPermissions[ "dumpLogs" ] = [ "navigate" ];

        settings.adminRoles.sysAdmin.append( "dumpLogs.*" );
    }
}