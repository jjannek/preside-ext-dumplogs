# Preside Extension: Dump logs

This is an extension for [Preside](https://www.presidecms.com) to provide the ability to log data dumps.
Visually it is similar to the existing core functionality of Error logs.

In detail the extension includes the following:

* Possibility to dump data (think "writeDump" / "cfdump") to a file that can be examined
* Preside Admin > System > Dump Logs
* Service DumpLogService with method _dumplog_ (pass in whatever you like)
* helper method _dumplog_ (use it in you handlers and views)

## Usage

Within a handler or view, use the following

    dumpLog( myvar );
    ...
    dumpLog( myvar=myvar );
    ...
    dumpLog( a=1, b=server, c=myvar );
    ...

Or use the service, e.g. within your own service
    
    component {

        property name="dumpLogService" inject="dumpLogService";

        function myFunction() {
            
            ...
            dumpLogService.dumpLog( myvar );
            ...
            dumpLogService.dumpLog( myvar=myvar );
            ...
            dumpLogService.dumpLog( a=1, b=server, c=myvar );
            ...
        }
    }

Head over to Preside Admin > System > Dump Logs to check the dumps there.

If you want to disable the feature, use the following in your Config.cfc

    settings.features.dumpLogs = false;

## Installation

Install the extension to your application via either of the methods detailed below (Git submodule / CommandBox) and then enable the extension by opening up the Preside developer console and entering:

    extension enable preside-ext-dumplogs
    reload all

### Git Submodule method

From the root of your application, type the following command:

    git submodule add https://bitbucket.org/jjannek/preside-ext-dumplogs.git application/extensions/preside-ext-dumplogs

### CommandBox (box.json) method

From the root of your application, type the following command:

    box install preside-ext-dumplogs

## Contribution

Feel free to fork and pull request. Any other feedback is also welcome - preferable on the PresideCMS slack channel.